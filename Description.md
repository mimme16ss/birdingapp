# BirdingApp

Diese Dokumentation beschreibt eine exemplarische Lösung für die [erste Übungsaufgabe](http://birding.regensburger-forscher.de) des MME-Kurses im Sommersemester 2016. Den Quellcode sowie eine Anleitung zur Verwendung finden Sie in [diesem Git-Repository](https://bitbucket.org/mimme16ss/birdingapp). Die einzelnen Module sind hier dokumentiert und beschrieben:

* [BirdingApp](BirdingApp.html)
* [BirdingModel](BirdingApp.BirdingModel.html)
* [BirdingController](BirdingApp.BirdingController.html)
* [BirdCounterView](BirdingApp.BirdCounterView.html)
* [BirdListView](BirdingApp.BirdListView.html)

Alexander Bazo <alexander.bazo@ur.de>