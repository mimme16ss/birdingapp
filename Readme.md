# BirdingApp

## Abhängigkeiten (Dependencies)

* *Font Awesome* [http://fontawesome.io/?utm_source=hackernewsletter]
* *underscore.js* [http://underscorejs.org/]
* *Node.js* [https://nodejs.org/en/]

## Build

1. Erstellen Sie einen Ordner `dependencies` im Projektverzeichnis
2. Kopieren Sie eine aktuelle Version von *Font Awesome* in den `dependencies`-Ordner. Bennen Sie den Ordner mit den *Font Awesome*-Dateien in `font-aweseome` um.
3. Kopien Sie eine aktuelle Version von *underscore.js* in den `dependencies`-Ordner. Benenen Sie die Datei in `underscore-min.js` um.
4. Führen Sie `npm install` im Projektverzeichnis aus

Anschließend kann die Live-Version mit `grunt`, und die Demo-Version mit `grunt demo` gebaut werden. Die fertigen Ergebnisse finden sich im `build`-Ordner. Eine Dokumentation der Anwendung und ihrer Module kann über den Befehl `grunt docs` erstellt werden. 

