/* global _, BirdingApp */

/**
 * @namespace BirdingController
 * @memberOf! BirdingApp
 * @description Controller zur Umsetzung der Nutzer-Interaktion mit dem User Interface.
 * <p>Der <code>BirdingController</code> fängt die Interaktion-Events der Benutzeroberfläche ab und reicht diese an registrierte Observer weiter.</p>
 * <p>Options-Objekt:</p>
 * <ul>
 * <li><code>options.searchInput</code> DOM-Referenz auf das Suchfenster</li>
 * <li><code>options.birdList</code> DOM-Referenz auf die Artenliste</li>
 * <li><code>options.counterList</code> DOM-Referenz auf die Dashboard-Liste</li>
 * </ul>
 */
BirdingApp.BirdingController = function(options) {
    var that = {},
        countCallbacks = [],
        removeCallbacks = [],
        addCallbacks = [],
        searchCallbacks = [],
        birdList,
        searchInput,
        counterList;


    function onSearchInputChanged(event) {
        _.each(searchCallbacks, function(callback) {
            callback(event.target.value);
        });
    }

    function onBirdAddedListener(event) {
        var birdId, listEntry = findParentWithClass(event.target, "bird-list-entry");
        birdId = listEntry.getAttribute("bird-id");
        _.each(addCallbacks, function(callback) {
            callback(birdId);
        });
    }

    function onBirdClickedListener(event) {
        var birdId, listEntry;
        if (!event.target.classList.contains("button")) {
            return;
        }
        listEntry = findParentWithClass(event.target, "bird-list-entry");
        birdId = listEntry.getAttribute("bird-id");
        if (event.target.classList.contains("increase")) {
            _.each(countCallbacks, function(callback) {
                callback(birdId);
            });
        } else {
            _.each(removeCallbacks, function(callback) {
                callback(birdId);
            });
        }

    }

     /**
     * @function findParentWithClass
     * @parameter
     * @memberof! BirdingApp.BirdingController   
     * @instance
     * @param {Objekt} child DOM-Referenz des Kindlements, dessen Elternelement gesucht werden soll.
     * @param {String} targetClass CSS-Klasse anhand derer das Elternelement identifiziert werden soll.
     * @description Sucht nach dem nächsten Elternelement von <code>child</code>, das die Klasse <code>targetClass</code> hat.
     */
    function findParentWithClass(child, targetClass) {
        var parent = child.parentElement;
        while (!parent.classList.contains(targetClass)) {
            parent = parent.parentElement;
            if (parent === undefined) {
                break;
            }
        }
        return parent;
    }

     /**
     * @function init
     * @public
     * @memberof! BirdingApp.BirdingController   
     * @instance
     * @description Initialisiert den Controller. Die übergebenen Referenzen werden in Variablen gespeichert und die notwendigen Listener zum Abfangen der Benutzerinteraktion werden registriert.
     */
    function init() {
        searchInput = options.searchInput;
        searchInput.addEventListener("input", onSearchInputChanged);
        birdList = options.birdList;
        birdList.addEventListener("click", onBirdAddedListener);
        counterList = options.counterList;
        counterList.addEventListener("click", onBirdClickedListener);
        return that;
    }
    
     /**
     * @function setOnSearchTermChangedListener
     * @public
     * @memberof! BirdingApp.BirdingController   
     * @instance
     * @param {Function} callback Callback-Funktion
     * @description Registriert einen neuen Callback bzw. Listener, der aufgerufen wird, wenn der Benutzer den Inhalt des Suchfensters verändert.
     */
    function setOnSearchTermChangedListener(callback) {
        searchCallbacks.push(callback);
    }

     /**
     * @function setOnBirdAddedListener
     * @public
     * @memberof! BirdingApp.BirdingController   
     * @instance
     * @param {Function} callback Callback-Funktion
     * @description Registriert einen neuen Callback bzw. Listener, der aufgerufen wird, wenn der Benutzer auf das Plus-Symbol eines Vogels in der Artenliste klickt.
     */
    function setOnBirdAddedListener(callback) {
        addCallbacks.push(callback);
    }

     /**
     * @function setOnBirdCountedListener
     * @public
     * @memberof! BirdingApp.BirdingController   
     * @instance
     * @param {Function} callback Callback-Funktion
     * @description Registriert einen neuen Callback bzw. Listener, der aufgerufen wird, wenn der Benutzer auf das Plus-Symbol eines Vogels im Dashboard klickt.
     */
    function setOnBirdCountedListener(callback) {
        countCallbacks.push(callback);
    }

     /**
     * @function setOnBirdRemovedListener
     * @public
     * @memberof! BirdingApp.BirdingController   
     * @instance
     * @param {Function} callback Callback-Funktion
     * @description Registriert einen neuen Callback bzw. Listener, der aufgerufen wird, wenn der Benutzer auf das Minus-Symbol eines Vogels im Dashboard klickt.
     */
    function setOnBirdRemovedListener(callback) {
        removeCallbacks.push(callback);
    }

    that.init = init;
    that.setOnSearchTermChangedListener = setOnSearchTermChangedListener;
    that.setOnBirdAddedListener = setOnBirdAddedListener;
    that.setOnBirdCountedListener = setOnBirdCountedListener;
    that.setOnBirdRemovedListener = setOnBirdRemovedListener;
    return that;
};
