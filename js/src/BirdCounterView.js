/* global _, BirdingApp */

/**
 * @namespace BirdCounterView
 * @memberOf! BirdingApp
 * @description View zur Darstellung des Dashboards mit den Vögeln, die zur Zählliste hinzugefügt wurden.
 * <p>Das <code>BirdCounterView</code> rendert das Dashboard </p>
 * <p>Options-Objekt:</p>
 * <ul>
 * <li><code>options.birdList</code> DOM-Referenz auf den Listen-Container für die Dashboard-Einträge</li>
 * <li><code>options.entryTemplateContent</code> Template-String für neue Listeneinträge</li>
 * </ul>
 */
BirdingApp.BirdCounterView = function(options) {
    var that = {},
        createEntryTemplate,
        birdList;

    /**
     * @function init
     * @public
     * @memberof! BirdingApp.BirdCounterView   
     * @instance
     * @description Initialisiert den View. Der übergebenene Template-String wird für die weitere Verwendung vorbereitet und wie die Referenz auf den Listen-Container in einer Variable gespeichert. 
     */
    function init() {
        createEntryTemplate = _.template(options.entryTemplateContent);
        birdList = options.birdList;
        return that;
    }

    /**
     * @function addCounterEntry
     * @public
     * @memberof! BirdingApp.BirdCounterView   
     * @instance
     * @param {Object} bird Vogel-Objekt, das zur Liste hinzugefügt werden soll.
     * @description Fügt den als Objekt repräsentierten und als Parameter übergebenen Vogel zum Dashboard hinzu. Das übergebene Objekt wird zusammen mit dem Template verwendet, um ein neues DOM-Element für die Liste zu erstellen. Zu Beginn wird geprüft, ob bereits ein DOM-Objekt für den Vogel mit der im Parameter-Objekt übergebenen ID vorhanden ist.
     */
    function addCounterEntry(bird) {
        var entryNode, existingNode = birdList.querySelector("[bird-id='" + bird.id + "']");
        if (existingNode !== null) {
            return;
        }
        entryNode = document.createElement("div");
        entryNode.innerHTML = createEntryTemplate(bird);
        birdList.appendChild(entryNode.children[0]);
    }

     /**
     * @function updateCounterEntry
     * @public
     * @memberof! BirdingApp.BirdCounterView   
     * @instance
     * @param {Object} bird Vogel-Objekt, dessen gezählte Anzahl im View aktualisiert werden soll.
     * @description Ändert die angezeigt Anzahl der gezählten Exemplare auf den Wert, der im Parameter-Objekt als <code>count</code>-Eigenschaft angeben ist.
     */
    function updateCounterEntry(bird) {
        var birdCounterNode = birdList.querySelector("[bird-id='" + bird.id + "'] .bird-current-max");
        birdCounterNode.innerHTML = bird.count;
    }

    that.init = init;
    that.addCounterEntry = addCounterEntry;
    that.updateCounterEntry = updateCounterEntry;
    return that;
};
