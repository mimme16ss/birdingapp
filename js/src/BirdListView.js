/* global _, BirdingApp */

/**
 * @namespace BirdListView
 * @memberOf! BirdingApp
 * @description View zur Darstellung der filterbaren Artenliste
 * <p>Das <code>BirdListView</code> verwaltet die filterbare Artenliste. </p>
 * <p>Options-Objekt:</p>
 * <ul>
 * <li><code>options.birdList</code> DOM-Referenz auf den Listen-Container</li>
 * <li><code>options.entryTemplateContent</code> Template-String für neue Listeneinträge</li>
 * </ul>
 */
BirdingApp.BirdListView = function(options) {
    var that = {},
        createEntryTemplate,
        birdList;

     /**
     * @function init
     * @public
     * @memberof! BirdingApp.BirdListView   
     * @instance
     * @description Initalisiert den View. Der übergebenene Template-String wird für die weitere Verwendung vorbereitet und wie die Referenz auf den Listen-Container in einer Variable gespeichert. 
     */
    function init() {
        createEntryTemplate = _.template(options.entryTemplateContent);
        birdList = options.birdList;
        return that;
    }

     /**
     * @function addListEntry
     * @public
     * @memberof! BirdingApp.BirdListView   
     * @instance
     * @param {Object} bird Vogel-Objekt, das zur Liste hinzugefügt werden soll
     * @description Fügt den als Objekt repräsentierten und als Parameter übergebenene Vogel zur Liste hinzu. Das übergebene Objekt wird zusammen mit dem Template verwendet, um ein neues DOM-Element für die Liste zu erstellen.
     */
    function addListEntry(bird) {
        var entryNode = document.createElement("div");
        entryNode.innerHTML = createEntryTemplate(bird);
        birdList.appendChild(entryNode.children[0]);
    }

     /**
     * @function clearList
     * @public
     * @memberof! BirdingApp.BirdListView   
     * @instance
     * @description Löscht alle Einträge aus der Liste.
     */
    function clearList() {
        while(birdList.firstChild) {
            birdList.removeChild(birdList.firstChild);
        }
    }

    that.init = init;
    that.addListEntry = addListEntry;
    that.clearList = clearList;
    return that;
};
