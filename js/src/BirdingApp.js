/**
 * @namespace BirdingApp
 * @description Zentrales Modul der Anwendung und Namespace-Objekt für alle anderen Module. 
 * <p>Model, Controller und Views werden hier erstellt und verwaltet. 
 * Events der unterschiedlichen Komponenten werden hier abgefangen und an die übrigen Komponenten verteilt.</p>
 */
var BirdingApp = (function() {
    var that = {},
        birdingModel,
        birdingController,
        birdCounterView,
        birdListView;

    /**
     * @function onSearchTermChanged
     * @private
     * @memberof! BirdingApp   
     * @instance
     * @param {string} term Suchstring
     * @description Callback-Methode, die aufgerufen wird, wenn der <code>Controller</code> feststellt, dass der Benutzer die Sucheingabe zum Filtern der Vogelliste geändert hat.
     */
    function onSearchTermChanged(term) {
        var index, birdList;
        if (term !== "") {
            birdList = birdingModel.filterBirdListByName(term);

        } else {
            birdList = birdingModel.getBirdList();
        }
        birdListView.clearList();
        for (index = 0; index < birdList.length; index++) {
            birdListView.addListEntry(birdList[index]);
        }
    }

    /**
     * @function onBirdAdded
     * @private
     * @memberof! BirdingApp   
     * @instance
     * @param {string} birdId ID des Vogels, der zum <code>CounterView</code> hinzugefügt werden soll.
     * @description Callback-Methode, die aufgerufen wird, wenn der <code>Controller</code> feststellt, dass der Benutzer einen Vogel zum <code>CounterView</code> hinzufügen möchte.
     */
    function onBirdAdded(birdId) {
        var bird = birdingModel.addBirdToCountingList(parseInt(birdId, 10));
        birdCounterView.addCounterEntry(bird);
    }

    /**
     * @function onBirdCounted
     * @private
     * @memberof! BirdingApp   
     * @instance
     * @param {string} birdId ID des Vogels, dessen gezählte Anzahl erhöht werden soll.
     * @description Callback-Methode, die aufgerufen wird, wenn der <code>Controller</code> feststellt, dass der Benutzer die Anzahl eines der zum <code>CounterView</code> hinzugefügten Vögel erhöhen möchte.
     */
    function onBirdCounted(birdId) {
        var bird = birdingModel.incrementBirdCount(parseInt(birdId, 10));
        birdCounterView.updateCounterEntry(bird);
    }

    /**  
     * @function onBirdRemoved
     * @private
     * @memberof! BirdingApp   
     * @instance
     * @param {string} birdId ID des Vogels, dessen Anzahl reduziert werden soll.
     * @description Callback-Methode, die aufgerufen wird, wenn der <code>Controller</code> feststellt, dass der Benutzer die Anzahl eines der zum <code>CounterView</code> hinzugefügten Vögel reduzieren möchte.
     */
    function onBirdRemoved(birdId) {
        var bird = birdingModel.decrementBirdCount(parseInt(birdId, 10));
        birdCounterView.updateCounterEntry(bird);

    }

    /**
     * @function init
     * @public
     * @memberof! BirdingApp   
     * @instance
     * @description Initialisierung der Anwendung und ihrer Komponenten. Die Methode dient als Einstiegspunkt (entry point) und wird in der Index-Datei aufgerufen. 
     */
    function init() {
        initBirdingModel();
        initBirdingController();
        initBirdCounterView();
        initBirdListView();
    }

    /**
     * @function initBirdingModel
     * @private
     * @memberof! BirdingApp   
     * @instance
     * @description Initialisiert das Model. Die JSON-formatierte Liste der Vögel wird aus dem Script-Tag ausgelesen und als Javascript-Array an das Model weitergegeben.
     */
    function initBirdingModel() {
        birdingModel = (new BirdingApp.BirdingModel({
            birdList: JSON.parse(document.querySelector("#bird-list").innerHTML)
        })).init();
    }

    /**
     * @function initBirdingController
     * @private
     * @memberof! BirdingApp   
     * @instance
     * @description Initialisiert den Controller und registriert anschließend verschiedene Listeners bei diesem. Dem Controller werden beim Initialisieren Referenzen auf die bereits selektierten und zu überwachenden DOM-Elemente übergeben. 
     */
    function initBirdingController() {
        birdingController = (new BirdingApp.BirdingController({
            searchInput: document.querySelector(".bird-search"),
            birdList: document.querySelector(".bird-gallery .bird-list"),
            counterList: document.querySelector(".bird-counter .bird-list")
        })).init();
        birdingController.setOnSearchTermChangedListener(onSearchTermChanged);
        birdingController.setOnBirdAddedListener(onBirdAdded);
        birdingController.setOnBirdCountedListener(onBirdCounted);
        birdingController.setOnBirdRemovedListener(onBirdRemoved);
    }

    /**
     * @function initBirdCounterView
     * @private
     * @memberof! BirdingApp   
     * @instance
     * @description Initialisiert den CounterView und übergibt dazu das zu verwaltende DOM-Element sowie das Template für neue Einträge.
     */
    function initBirdCounterView() {
        birdCounterView = (new BirdingApp.BirdCounterView({
            birdList: document.querySelector(".bird-counter .bird-list"),
            entryTemplateContent: document.querySelector("#bird-counter-entry").innerHTML
        })).init();
    }

    /**
     * @function initBirdListView
     * @private
     * @memberof! BirdingApp   
     * @instance
     * @description Initialisiert den BirdListView und übergibt dazu das zu verwaltende DOM-Element sowie das Template für neue Einträge.
     */
    function initBirdListView() {
        var index, birdList = birdingModel.getBirdList();
        birdListView = (new BirdingApp.BirdListView({
            birdList: document.querySelector(".bird-gallery .bird-list"),
            entryTemplateContent: document.querySelector("#bird-list-entry").innerHTML
        })).init();
        for (index = 0; index < birdList.length; index++) {
            birdListView.addListEntry(birdList[index]);
        }
    }

    that.init = init;
    return that;
}());
