/* global _, BirdingApp */

/**
 * @namespace BirdingModel
 * @memberOf! BirdingApp
 * @description Daten-Model der Anwendung
 * <p>Das <code>BirdingModel</code> verwaltet die Liste der in der Anwendung bekannten Vogelarten und speichert die Vögel, die bereits vom Nutzer gezählt wurden. 
 * Für jede Art wird im Model die aktuell gezählte Anzahl der Exemplare gespeichert. 
 * Zusätzlich erlaubt das Model die Rückgabe einer gefilterte Artenliste anhand eines übergebenen Suchstrings.</p>
 * <p>Options-Objekt:</p>
 * <ul>
 * <li><code>options.birdList</code> Array mit den bekannten Vogelarten</li>
 * </ul>
 */
BirdingApp.BirdingModel = function(options) {
    var that = {},
        birdList,
        countingList = [];

     /**
     * @function init
     * @public
     * @memberof! BirdingApp.BirdingModel   
     * @instance
     * @description Initialisiert das Model. Die übergebene Artenliste wird in <code>birdList</code> gespeichert und anschließend alphabetisch, nach den deutschen Namen der Arten sortiert.
     */
    function init() {
        birdList = options.birdList;
        birdList = _.sortBy(birdList, function(bird) {
            return bird.name;
        });
        return that;
    }

     /**
     * @function getBirdList
     * @public
     * @memberof! BirdingApp.BirdingModel   
     * @instance
     * @description Gibt die Liste der aktuell bekannten Vogelarten mit der Anzahl der jeweils gezählten Exemplare zurück.
     * @returns {Array} List der Vogelarten
     */
    function getBirdList() {
        return birdList;
    }

     /**
     * @function getFilterdBirdList
     * @private
     * @memberof! BirdingApp.BirdingModel   
     * @instance
     * @param {string} criteria Eigenschaft der Vogel-Objekte, nach der gefiltert werden soll
     * @param {string} query Suchstring, nach dem im Eigenschaftswert gesucht werden soll
     * @description Gibt eine gefilterte Liste der Vogelarten zurück. Eine Art wird in die Liste aufgenommen, wenn der <code>query</code>-String in der als <code>criteria</code> übergebenen Eigenschaft des jeweiligen Vogel-Objekts vorkommt.
     * Die Methode wird nicht direkt über das Modul zugänglich gemacht sondern kann über zwei Methoden, die im <code>that</code>-Objekt erstellt werden, genutzt werden.
     * @returns {Array} List der Vogelarten
     */
    function getFilterdBirdList(criteria, query) {
        return _.filter(birdList, function(bird) {
            return bird[criteria].toLowerCase().indexOf(query.toLowerCase()) !== -1;
        });
    }

     /**
     * @function getBirdById
     * @private
     * @memberof! BirdingApp.BirdingModel   
     * @instance
     * @param {Number} id Id des gesuchten Vogels
     * @description Gibt die Vogelart mit der als Parameter übergebenen ID zurück. Ist keine Art mit der ID vorhanden, wird <code>undefined</code> zurückgegeben.
     * @returns {Objekt} Objekt mit Beschreibung der Vogelart
     */
    function getBirdById(id) {
        return _.findWhere(birdList, { id: id });
    }

     /**
     * @function getBirdById
     * @public
     * @memberof! BirdingApp.BirdingModel   
     * @instance
     * @description Gibt eine Liste aller Vogelarten zurück, die bereits zur Zählliste hinzugefügt wurden.
     * @returns {Array} Liste der Vogelarten
     */
    function getCountingList() {
        var tmpList = _.filter(birdList, function(bird) {
            return _.contains(countingList, bird.id);
        });
        return tmpList;
    }

    /**
     * @function addBirdToCountingList
     * @public
     * @memberof! BirdingApp.BirdingModel   
     * @instance
     * @param {Number} id ID des Vogels, der zur Liste hinzugefügt werden soll
     * @description Fügt den Vogel mit der als Parameter <code>id</code> übergebenen ID zur Zählliste hinzu.
     * @returns {Objekt} Objekt mit Beschreibung der Vogelart, die zur Liste hinzugefügt wurde.
     */
    function addBirdToCountingList(id) {
        var target = getBirdById(id);
        if (_.contains(countingList, id)) {
            return getBirdById(id);
        } else {
            target.count = 0;
            countingList.push(id);
            return getBirdById(id);
        }
    }

    /**
     * @function changeBirdCounter
     * @private
     * @memberof! BirdingApp.BirdingModel   
     * @instance
     * @param {Number} increment Wert um den die Anzahl der gezählten Exemplare verändert werden soll
     * @param {id} id Id des Vogels, dessen gezählte Anzahl verändert werden soll
     * @description Ändert die Anzahl der gezählten Exemplare für die Vogelart mit der der als Parameter <code>id</code> übergebenen ID.
     * Die Methode wird nicht direkt aus dem Modul zugänglich gemacht, sondern kann über zwei Methoden, die im <code>that</code>-Objekt erstellt werden, genutzt werden.
     * @returns {Objekt} Objekt mit Beschreibung der Vogelart, deren Anzahl geändert wurde.
     */
    function changeBirdCounter(increment, id) {
        var target = getBirdById(id);
        if (target === undefined) {
            return target;
        }
        target.count = target.count + increment;
        if (target.count < 0) {
            target.count = 0;
        }
        return target;
    }

    that.init = init;
    that.getBirdList = getBirdList;
    that.filterBirdListByName = getFilterdBirdList.bind(this, "name");
    that.filterBirdListByLatinName = getFilterdBirdList.bind(this, "latinName");
    that.addBirdToCountingList = addBirdToCountingList;
    that.getCountingList = getCountingList;
    that.incrementBirdCount = changeBirdCounter.bind(this, 1);
    that.decrementBirdCount = changeBirdCounter.bind(this, -1);
    return that;
};
