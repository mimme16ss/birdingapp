module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        eslint: {
            src: ["js/src/*.js"],
        },
        clean: {
            build: "build/",
            docs: "docs/",
        },
        mkdir: {
            build: {
                options: {
                    create: ["build", "build/js/libs", "build/res/css", "build/res/img"],
                },
            },
        },
        cssmin: {
            build: {
                files: [{
                    expand: true,
                    cwd: "res/css",
                    src: ["styles.css"],
                    dest: "build/res/css",
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            options: {
                preserveComments: false,
            },
            build: {
                files: {
                    'build/js/app.min.js': ["js/src/BirdingApp.js", "js/src/BirdingModel.js", "js/src/BirdingController.js", "js/src/BirdCounterView.js", "js/src/BirdListView.js"],
                }
            }
        },
        copy: {
            build: {
                files: [{
                    expand: true,
                    src: ["index.html"],
                    dest: "build/",
                    filter: "isFile",
                }, {
                    expand: true,
                    src: ["LICENSE"],
                    dest: "build/",
                    filter: "isFile",
                }, {
                    expand: true,
                    cwd: "res/img",
                    src: ["*.png"],
                    dest: "build/res/img",
                    filter: "isFile",
                }, {
                    expand: true,
                    cwd: "dependencies",
                    src: ["underscore-min.js"],
                    dest: "build/js/libs/",
                    filter: "isFile",
                }, {
                    expand: true,
                    cwd: "dependencies",
                    src: ["font-awesome/*/*"],
                    dest: "build/res/",
                }, {
                    expand: true,
                    cwd: "dependencies",
                    src: ["fonts/*/*"],
                    dest: "build/res/",
                }, ],
            },
            demo: {
                files: [{
                    expand: true,
                    src: ["index_demo.html"],
                    dest: "build/",
                    filter: "isFile",
                }, {
                    expand: true,
                    cwd: "js/src/",
                    src: ["*.js"],
                    dest: "build/js/src/",
                }, ],
            },
        },
        rename: {
            demo: {
                files: [
                    { src: ["build/index_demo.html"], dest: "build/index.html" },
                ]
            }
        },
        jsdoc: {
            docs: {
                src: ["Description.md", "js/src/**/*.js"],
                options: {
                    destination: "docs",
                    access: "all"
                }
            }
        },
    });

    grunt.loadNpmTasks("gruntify-eslint");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-mkdir");
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-rename');
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.registerTask("default", ["eslint", "clean:build", "mkdir:build", "cssmin:build", "uglify:build", "copy:build"]);
    grunt.registerTask("demo", ["eslint", "clean:build", "mkdir:build", "cssmin:build", "copy:build", "copy:demo", "rename:demo"]);
    grunt.registerTask("docs", ["clean:docs", "jsdoc:docs"]);

};
